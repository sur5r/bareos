Source: bareos
Section: admin
Priority: optional
Maintainer: Bareos Packaging Team <team+bareos@tracker.debian.org>
Uploaders:
 Dominik George <natureshadow@debian.org>,
Build-Depends:
 acl-dev,
 bc,
 chrpath,
 debhelper (>= 11),
 default-libmysqlclient-dev | libmysqlclient-dev,
 dh-exec (>= 0.13),
 dpkg-dev (>= 1.13.19),
 libacl1-dev,
 libcap-dev [linux-any],
 libcephfs-dev [linux-any],
 libcmocka-dev (>= 1.0.1),
 libglusterfs-dev [linux-any],
 libgnutls28-dev,
 libjansson-dev,
 liblzo2-dev,
 libpq-dev,
 libqt4-dev,
 librados-dev [linux-any],
 libradosstriper-dev [linux-any],
 libreadline-dev,
 libsqlite3-dev,
 libwrap0-dev,
 libx11-dev,
 logrotate,
 lsb-release,
 mtx [!hurd-any],
 ncurses-dev,
 pkg-config,
 po-debconf (>= 0.8.2),
 python-dev,
 zlib1g-dev,
Standards-Version: 4.3.0
Vcs-Git: https://salsa.debian.org/pkg-bareos-team/bareos.git
Vcs-Browser: https://salsa.debian.org/pkg-bareos-team/bareos
Homepage: http://www.bareos.org/

Package: bareos
Architecture: any
Depends:
 bareos-client (= ${binary:Version}),
 bareos-director (= ${binary:Version}),
 bareos-storage (= ${binary:Version}),
 ${misc:Depends},
Description: Backup Archiving Recovery Open Sourced - metapackage
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 It is efficient and relatively easy to use, while offering many advanced
 storage management features that make it easy to find and recover lost or
 damaged files. Due to its modular design, Bareos is scalable from small
 single computer systems to networks of hundreds of machines.
 .
 This metapackage installs the entire suite of Bareos applications: job
 scheduling, storage control, node connector, and administrative console.

Package: bareos-bconsole
Architecture: any
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-common (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Conflicts:
 bacula-console,
Replaces:
 bacula-console,
Description: Backup Archiving Recovery Open Sourced - text console
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 The management console allows the administrator or user to
 communicate with the Bareos Director.
 .
 This package provides the text-interface version of the console.

Package: bareos-client
Architecture: any
Depends:
 bareos-bconsole (>= ${binary:Version}),
 bareos-filedaemon (>= ${binary:Version}),
 ${misc:Depends},
Suggests:
 bareos-traymonitor,
Description: Backup Archiving Recovery Open Sourced - client metapackage
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 The package is a metapackage for client installations (file daemon and
 console).

Package: bareos-common
Architecture: any
Pre-Depends:
 adduser,
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 openssl,
 ${misc:Depends},
 ${shlibs:Depends},
Conflicts:
 bacula-common,
 bacula-director-common,
Description: Backup Archiving Recovery Open Sourced - common files
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides files that are useful for other Bareos packages.

Package: bareos-database-common
Architecture: any
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-common (= ${binary:Version}),
 bareos-database-postgresql (= ${binary:Version}) | bareos-database-mysql (= ${binary:Version}) | bareos-database-sqlite3 (= ${binary:Version}),
 dbconfig-common,
 lsb-base (>= 3.2-13),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Backup Archiving Recovery Open Sourced - common catalog files
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides generic abstraction libs and files to connect the Bareos
 Director daemon to a database.

Package: bareos-database-postgresql
Architecture: any
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 lsb-base (>= 3.2-13),
 postgresql-client,
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 postgresql,
Description: Backup Archiving Recovery Open Sourced - PostgreSQL backend
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides the functionality to connect the Bareos Director
 daemon to a PostgreSQL database.

Package: bareos-database-mysql
Architecture: any
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 lsb-base (>= 3.2-13),
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 default-mysql-server | virtual-mysql-server,
Description: Backup Archiving Recovery Open Sourced - MySQL backend
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides the functionality to connect the Bareos Director
 daemon to a MySQL compatible database.

Package: bareos-database-sqlite3
Architecture: any
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 lsb-base (>= 3.2-13),
 sqlite3,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Backup Archiving Recovery Open Sourced - SQLite backend
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides the functionality to connect the Bareos Director
 daemon to a SQLite database.

Package: bareos-database-tools
Architecture: any
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-common (= ${binary:Version}),
 bareos-database-common (= ${binary:Version}),
 lsb-base (>= 3.2-13),
 ${misc:Depends},
 ${shlibs:Depends},
Conflicts:
 bacula-sd-mysql,
 bacula-sd-pgsql,
 bacula-sd-sqlite3,
Description: Backup Archiving Recovery Open Sourced - database tools
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides tools that requires access to the Bareos database.

Package: bareos-devel
Architecture: any
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 ${misc:Depends},
 ${shlibs:Depends},
Description: Backup Archiving Recovery Open Sourced - development files
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides additional files for develop against Bareos.

Package: bareos-director
Architecture: any
Pre-Depends:
 adduser,
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-common (= ${binary:Version}),
 bareos-database-common (= ${binary:Version}),
 bareos-database-tools,
 bsd-mailx | mailx,
 lsb-base (>= 3.2-13),
 lsof,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 logrotate,
Conflicts:
 bacula-director,
Replaces:
 bacula-director,
Description: Backup Archiving Recovery Open Sourced - director daemon
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 The Bareos Director service supervises all the backup, restore, verify and
 archive operations. It can run as a daemon or as a foreground service which
 administrators can use to schedule backups and recover files.
 .
 This package contains the Bareos Director daemon.

Package: bareos-filedaemon
Architecture: any
Pre-Depends:
 adduser,
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-common (= ${binary:Version}),
 lsb-base (>= 3.2-13),
 lsof,
 ${misc:Depends},
 ${shlibs:Depends},
Conflicts:
 bacula-fd,
Replaces:
 bacula-fd,
Description: Backup Archiving Recovery Open Sourced - file daemon
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 The file daemon has to be installed on the machine to be backed up. It is
 responsible for providing the file attributes and data when requested by
 the Director, and also for the file system-dependent part of restoration.
 .
 This package contains the Bareos File daemon.

Package: bareos-storage
Architecture: any
Pre-Depends:
 adduser,
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-common (= ${binary:Version}),
 lsb-base (>= 3.2-13),
 lsof,
 ${misc:Depends},
 ${shlibs:Depends},
Conflicts:
 bacula-sd,
Replaces:
 bacula-sd,
Breaks:
 bareos-storage-tape (<< 14.2.6~),
Description: Backup Archiving Recovery Open Sourced - storage daemon
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 The storage daemon performs the storage and recovery of the file
 attributes and data to the physical media; in other words, it is
 responsible for reading and writing the backups.
 .
 It runs on the machine which has access to the backup device(s) - usually
 a tape drive, but alternatively other storage media, such as files.

Package: bareos-storage-fifo
Architecture: any
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-common (= ${binary:Version}),
 bareos-storage (= ${binary:Version}),
 lsb-base (>= 3.2-13),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Backup Archiving Recovery Open Sourced - storage daemon FIFO backend
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package contains the Storage backend for FIFO files.
 This package is only required, when a resource "Archive Device = fifo"
 should be used by the Bareos Storage Daemon.

Package: bareos-storage-tape
Architecture: any
Pre-Depends:
 adduser,
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-common (= ${binary:Version}),
 bareos-storage (= ${binary:Version}),
 lsb-base (>= 3.2-13),
 mtx,
 ${misc:Depends},
 ${shlibs:Depends},
Recommends:
 mt-st,
Suggests:
 scsitools,
 sg3-utils,
Breaks:
 bareos-storage (<< 14.2.6~),
Replaces:
 bareos-storage (<< 14.2.6~),
Description: Backup Archiving Recovery Open Sourced - storage daemon tape support
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 The storage daemon performs the storage and recovery of the file
 attributes and data to the physical media; in other words, it is
 responsible for reading and writing the backups.
 .
 It runs on the machine which has access to the backup device(s) - usually
 a tape drive, but alternatively other storage media, such as files.
 .
 This adds the tape specific support files for the storage daemon.

Package: bareos-tools
Architecture: any
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-common (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Conflicts:
 bacula-sd,
 bacula-sd-mysql,
 bacula-sd-pgsql,
 bacula-sd-sqlite3,
Description: Backup Archiving Recovery Open Sourced - common tools
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides some additional tools, mostly to work directly with
 Bareos storage volumes.

Package: bareos-director-python-plugin
Architecture: any
Section: python
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-common (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Backup Archiving Recovery Open Sourced - director Python plugin
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides the Python plugin for the director.

Package: bareos-filedaemon-ceph-plugin
Architecture: linux-any
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-filedaemon (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Backup Archiving Recovery Open Sourced - file daemon CEPH plugin
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides the CEPH plugins for the file daemon.

Package: bareos-filedaemon-glusterfs-plugin
Architecture: linux-any
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-filedaemon (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Backup Archiving Recovery Open Sourced - filedaemon-glusterfs-plugin
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides the glusterfs plugin for the file daemon.

Package: bareos-filedaemon-python-plugin
Architecture: any
Section: python
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-common (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Backup Archiving Recovery Open Sourced - file daemon Python plugin
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides the Python plugin for the filedaemon.

Package: bareos-filedaemon-ldap-python-plugin
Architecture: any
Section: python
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-common (= ${binary:Version}),
 bareos-filedaemon-python-plugin (= ${binary:Version}),
 python-ldap,
 ${misc:Depends},
 ${shlibs:Depends},
Description: Backup Archiving Recovery Open Sourced - file daemon LDAP plugin
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides the LDAP Python plugin for the filedaemon.

Package: bareos-storage-ceph
Architecture: linux-any
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-storage (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Backup Archiving Recovery Open Sourced - storage daemon CEPH backend
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides the CEPH backend for the storage daemon.

Package: bareos-storage-glusterfs
Architecture: linux-any
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-storage (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Backup Archiving Recovery Open Sourced - storage-glusterfs-plugin
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides the glusterfs backend for the storage daemon.

Package: bareos-storage-python-plugin
Architecture: any
Section: python
Pre-Depends:
 debconf (>= 1.4.30) | debconf-2.0,
Depends:
 bareos-common (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Description: Backup Archiving Recovery Open Sourced - storage-python-plugin
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides the Python plugin for the storage daemon.

Package: bareos-traymonitor
Architecture: any
Depends:
 bareos-common (= ${binary:Version}),
 ${misc:Depends},
 ${shlibs:Depends},
Suggests:
 kde | gnome-desktop-environment,
Conflicts:
 bacula-traymonitor,
Replaces:
 bacula-traymonitor,
Description: Backup Archiving Recovery Open Sourced - tray monitor
 Bareos is a set of programs to manage backup, recovery and verification of
 data across a network of computers of different kinds.
 .
 This package provides a tray monitor for the Bareos backup system. It is
 FreeDesktop-compatible, which means it will work under both KDE and
 GNOME.
 .
 The monitor constantly displays the status of Bareos.
